package main

import (
	"strings"
	"testing"
	"time"
)

func TestGetUnitedChan(t *testing.T) {
	// test-1
	chs := make([]chan string, 5)
	for i := range chs {
		sz := 4 - i
		chs[i] = make(chan string, sz)
	}

	go func() {
		chs[3] <- "тапки"
		close(chs[3])
		time.Sleep(time.Millisecond * 33)

		chs[2] <- "плавки"
		time.Sleep(time.Millisecond * 33)

		chs[0] <- "смокинг"
		time.Sleep(time.Millisecond * 33)

		chs[4] <- "очки"
		close(chs[4])
		time.Sleep(time.Millisecond * 33)

		chs[2] <- "маска"
		time.Sleep(time.Millisecond * 33)

		chs[2] <- "крем"
		close(chs[1])
		close(chs[2])
		time.Sleep(time.Millisecond * 33)

		chs[0] <- "парашют"
		close(chs[0])
	}()

	results := make([]string, 0, 8)

	startFuncCall := time.Now()
	unitedChan := getUnitedChan(chs...)
	lockDur := time.Since(startFuncCall)
	if lockDur > (time.Millisecond * 10) {
		t.Errorf("it seems that the function have some locks for about %v", lockDur)
	}

	for s := range unitedChan {
		results = append(results, s)
	}
	if strings.Join(results, " ") != strings.Join([]string{
		"тапки",
		"плавки",
		"смокинг",
		"очки",
		"маска",
		"крем",
		"парашют",
	}, " ") {
		t.Errorf("test-1 failed")
	}

	// test-2
	ch1 := make(chan string)
	ch2 := make(chan string, 1)
	ch3 := make(chan string, 7)

	go func() { ch1 <- "как"; close(ch1) }()
	ch2 <- "однажды"
	ch3 <- "Жак"
	ch3 <- "звонaрь"
	ch3 <- "голoвою"
	ch3 <- "сбил"
	ch3 <- "фонарь"
	close(ch2)
	close(ch3)

	resultMap := make(map[string]int, 8)
	for s := range getUnitedChan(ch1, ch2, ch3) {
		if _, ok := resultMap[s]; !ok {
			resultMap[s] = 0
		}
		resultMap[s]++
	}
	expectedKeys := strings.Split(
		"как однажды Жак звонaрь голoвою сбил фонарь",
		" ",
	)
	for _, k := range expectedKeys {
		if val := resultMap[k]; val != 1 {
			t.Errorf("test-2 failed: msg %s occurs %d times", k, val)
		}
		delete(resultMap, k)
	}
	if len(resultMap) != 0 {
		t.Errorf("test-2 failed: unexpected messages: %v", resultMap)
	}
}
